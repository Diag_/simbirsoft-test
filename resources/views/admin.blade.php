@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Загруженные файлы</div>

                <div class="card-body">
					<table class="table">
						<thead>
							<th>Имя файла</th>
							<th>E-mail</th>
							<th></th>
							<th></th>
						</thead>
						<tbody>
							@if ($files->total() == 0)
							<tr>
								<td colspan="4">Нет загруженных файлов</td>
							</tr>
							@endif
							
							@foreach ($files as $file)
							<tr>
								<td>
									<a href="{{ $file->getUrl() }}">{{ $file->name }}</a>
								</td>
								<td>{{ $file->user_email }}</td>
								<td>
									<a href="{{ route('file_update', ['id' => $file->id]) }}">Изменить</a>
								</td>
								<td>
									<a href="{{ route('file_delete', ['id' => $file->id]) }}" class="text-danger file-delete_link">Удалить</a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
                </div>
				<div class="card-footer">
					{{ $files->links() }}
				</div>
            </div>
        </div>
    </div>
</div>
<form method="POST" id="file-delete_form">
	@csrf
</form>
<script>
	$(function () {
		$('.file-delete_link').on('click', function (e) {
			e.preventDefault();
			
			if (confirm('Вы действительно хотите удалить этот файл?')) {
				$('#file-delete_form').attr('action', $(this).attr('href')).submit();
			}
		});
	});
</script>
@endsection
