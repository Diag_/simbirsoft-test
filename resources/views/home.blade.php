@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Загрузка файла</div>
                <div class="card-body">					
					@if (Session::has('file_status'))
					<div class="alert alert-info">{{ Session::get('file_status') }}</div>
					@endif
					<form method="POST" action="{{ route('upload-file') }}" enctype="multipart/form-data">
						@csrf

						<div class="form-group">
							<label>Выберите файл</label>
							<div>
								<input type="file" name="file" class="form-control-file{{ $errors->has('file') ? ' is-invalid' : '' }}"/>

								@if ($errors->has('file'))
								<span class="invalid-feedback" role="alert">
									<strong>{{ $errors->first('file') }}</strong>
								</span>
								@endif
							</div>
						</div>
						<div class="form-group">
							<label>Описание файла</label>
							<textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">{{ old('description') }}</textarea>

							@if ($errors->has('description'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('description') }}</strong>
							</span>
							@endif
						</div>
						<div class="form-group">
							<label>E-mail</label>
							<input type="text" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}"/>

							@if ($errors->has('email'))
							<span class="invalid-feedback" role="alert">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
							@endif
						</div>
						<button type="submit" class="btn btn-primary float-right">
							Отправить
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
