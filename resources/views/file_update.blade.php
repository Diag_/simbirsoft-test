@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			<a href="/admin">К списку файлов</a>
			<br/>
			<br/>
            <div class="card">
                <div class="card-header">Данные о файле</div>
                <div class="card-body">					
					<form method="POST" action="{{ route('file_update', ['id' => $file->id]) }}">
						@csrf

						<div class="form-group">
							<label>Файл</label>
							<div>
								<a href="{{ $file->getUrl() }}">{{ $file->name }}</a>
							</div>
						</div>
						<div class="form-group">
							<label>Описание файла</label>
							<textarea class="form-control" name="description">{{ $file->description }}</textarea>
						</div>
						<div class="form-group">
							<label>E-mail</label>
							<input type="text" name="email" class="form-control" value="{{ $file->user_email }}" disabled/>
						</div>
						<button type="submit" class="btn btn-success float-right">
							Сохранить
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
