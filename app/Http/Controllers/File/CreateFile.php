<?php

namespace App\Http\Controllers\File;

use Illuminate\Http\Request;
use App\File;

trait CreateFile {
	
	public function create(Request $request) {
		$this->validate($request, [
			'file' => 'required|min:100|max:1000',
			'email' => 'required|email',
			'description' => 'required|min:5|max:300',
		]);
		
		$file = $request->file('file');
		
		$model = new File;
		$model->hash = str_random(32);
		$model->name = $file->getClientOriginalName();
		$model->description = $request->get('description');
		$model->user_hash = $request->cookies->get('user_hash');
		$model->user_email = $request->get('email');
		$model->save();
		
		$file->move('files', $model->hash . '.' . $file->getClientOriginalExtension());
		
		$request->session()->flash('file_status', 'Файл был успешно загружен! В скором времени вы получите письмо с информацией о файле.');
		
		return redirect('/');
	}
}
