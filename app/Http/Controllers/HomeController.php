<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request) 
	{
		$content = view('home');
		
		if (!$request->cookies->has('user_hash')) {
			return response($content)->cookie('user_hash', str_random(), 1440);
		}
		return $content;
	}

}
