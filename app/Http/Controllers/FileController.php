<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\File;
use App\Mail\FileNotify;
use Illuminate\Support\Facades\Mail;

class FileController extends Controller {

	public function create(Request $request) {
		$this->validate($request, [
			'file' => 'required|min:100|max:1000',
			'email' => 'required|email',
			'description' => 'required|min:5|max:300',
		]);

		$file = $request->file('file');

		$model = new File;
		$model->hash = str_random(32);
		$model->name = $file->getClientOriginalName();
		$model->description = $request->get('description');
		$model->user_hash = $request->cookies->get('user_hash');
		$model->user_email = $request->get('email');
		$model->save();

		$file->move('files', $model->hash . '.' . $file->getClientOriginalExtension());

		$this->sendEmail($model->user_email, $model);

		$request->session()->flash('file_status', 'Файл был успешно загружен! В скором времени вы получите письмо с информацией о файле.');

		return redirect('/');
	}

	protected function sendEmail($address, File $file) {
		$text = 'Ссылка на ваш файл: <a href="' . $file->getUrl() . '">' . $file->name . '</a>';

		Mail::raw($text, function ($message) use ($address) {
			$message->from('info@filestorage.local', 'Файловое хранилище');
			$message->to($address);
		});
	}

	public function update(File $file, Request $request) {
		$inputs = Input::all();

		if ($request->isMethod('POST') && !empty($inputs) && $file->update($inputs)) {
			return redirect('/admin');
		}
		return view('file_update', ['file' => $file]);
	}

	public function delete(File $file) {
		$file->delete();
		return redirect('/admin');
	}

}
