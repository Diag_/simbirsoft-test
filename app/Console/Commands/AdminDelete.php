<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;

class AdminDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:delete {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Удаление администратора';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        User::where('email', $this->argument('email'))->delete();
    }
}
