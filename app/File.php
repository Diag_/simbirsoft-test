<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
	protected $fillable = ['description'];
	
    public function getUrl() 
	{
		return route('file_url', ['user_hash' => $this->user_hash, 'file_hash' => $this->hash]);
	}
	
	public function getPath() 
	{
		return public_path() . '/files/' . $this->hash . '.' . pathinfo($this->name, PATHINFO_EXTENSION);
	}
	
}
