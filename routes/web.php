<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'HomeController@index');

Route::group(['prefix' => 'api'], function () {
	Route::post('create', 'FileController@create')->name('upload-file');
	Route::match(['get', 'post'], 'update/{id}', ['uses' => 'FileController@update'])->middleware('auth')->name('file_update');
	Route::post('delete/{id}', ['uses' => 'FileController@delete'])->middleware('auth')->name('file_delete');
});

Route::get('/uploads/{user_hash}/{file_hash}', function ($user_hash, $file_hash) {
	$file = App\File::where('hash', $file_hash)->where('user_hash', $user_hash)->firstOrFail();
	return Response::download($file->getPath(), $file->name);
})->name('file_url');

Auth::routes();

Route::get('/admin', 'AdminController@index')->name('home');
